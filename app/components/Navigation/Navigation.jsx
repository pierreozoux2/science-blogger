import React from 'pubsweet-core/node_modules/react'
import { LinkContainer } from 'pubsweet-core/node_modules/react-router-bootstrap'
import { Navbar, Nav, NavItem, NavbarBrand } from 'pubsweet-core/node_modules/react-bootstrap'

import AuthHelper from 'pubsweet-core/app/helpers/AuthHelper'
import NavbarUser from 'pubsweet-core/app/components/Navigation/NavbarUser'

export default class Navigation extends React.Component {

  render () {
    const { actions, auth } = this.props
    let logoutButtonIfAuthenticated
    if (auth.isAuthenticated) {
      logoutButtonIfAuthenticated = <NavbarUser
        roles={auth.roles}
        username={auth.username}
        switchRole={actions.switchRole}
        onLogoutClick={actions.logoutUser}
      />
    }
    return (
      <Navbar fluid>
        <Navbar.Header>
          <NavbarBrand>
            <a href='#'><img src='/pubsweet.jpg' alt='science'/></a>
          </NavbarBrand>
        </Navbar.Header>
        <Nav eventKey={0}>
          <LinkContainer to='/manage/posts'>
            <NavItem>Science Posts</NavItem>
          </LinkContainer>
          { AuthHelper.showForUser(auth, 'users') &&
            <LinkContainer to='/manage/users'>
              <NavItem>Users</NavItem>
            </LinkContainer>
          }
        </Nav>
        { logoutButtonIfAuthenticated }
      </Navbar>
    )
  }
}

Navigation.propTypes = {
  actions: React.PropTypes.object.isRequired,
  auth: React.PropTypes.object
}
