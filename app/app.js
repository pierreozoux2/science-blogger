import React from 'pubsweet-core/node_modules/react'
import ReactDOM from 'pubsweet-core/node_modules/react-dom'

import configureStore from 'pubsweet-core/app/store/configureStore'
import Root from 'pubsweet-core/app/components/Root'

import { AppContainer } from 'pubsweet-core/node_modules/react-hot-loader'
import { browserHistory } from 'pubsweet-core/node_modules/react-router'
import { syncHistoryWithStore } from 'pubsweet-core/node_modules/react-router-redux'

let store = configureStore(browserHistory, {})
let history = syncHistoryWithStore(browserHistory, store)

const rootEl = document.getElementById('root')

ReactDOM.render(
  <AppContainer>
    <Root store={store} history={history}/>
  </AppContainer>,
  rootEl
)

if (module.hot) {
  module.hot.accept('pubsweet-core/app/components/Root', () => {
    const NextRoot = require('pubsweet-core/app/components/Root').default
    ReactDOM.render(
      <AppContainer>
        <NextRoot store={store} history={history}/>
      </AppContainer>,
      rootEl
    )
  })
}
