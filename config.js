module.exports = {
  secret: 'EXAMPLEDONTUSE',
  API_ENDPOINT: '/api',
  theme: 'PepperTheme',
  routes: 'app/routes.jsx',
  navigation: 'app/components/Navigation/Navigation.jsx'
}
